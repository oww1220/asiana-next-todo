declare module 'Type_resData' {
    
    interface ListState {
        date: string;
        chdate: string;
        day: string;
        cid: string;
        sort: string;
        name: string;
        catagory: string;
        lang: string;
        koUrl: string;
        viUrl: string;
        esUrl: string;
        enUrl: string;
        jaUrl: string;
        chUrl: string;
        zhUrl: string;
        ruUrl: string;
        deUrl: string;
        frUrl: string;
        state: string;
        cording: string;
        request: string;
        etc: string;
        id: number;
        chk: string[];
        chkall?: boolean;
    }

    interface LangsLink {
        KO: boolean;
        VI: boolean;
        ES: boolean;
        EN: boolean;
        JA: boolean;
        CH: boolean;
        ZH: boolean;
        RU: boolean;
        DE: boolean;
        FR: boolean;
    }
}