export{}

declare global {
    interface Window{
        ClassicEditor: any;
        editor: any;
    }
    interface HTMLElement {
        
    }
    interface IObj {
        [key: string | number]: any;
    }

    interface IClassNames {
        [className: string]: string;
    }

    interface IErrors {
        /* The validation error messages for each field (key is the field name */
        [key: string]: string;
    }


    type PromiseCallback = (resolve: (value: any) => void, reject: (reason?: any) => void) => void;
}

