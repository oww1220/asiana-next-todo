const withBundleAnalyzer = require('@next/bundle-analyzer')({
    enabled: process.env.ANALYZE === 'true',
});

const withFonts = require('next-fonts');
const withImages = require('next-images');

module.exports = withBundleAnalyzer(withFonts());
module.exports = withBundleAnalyzer(withImages());

