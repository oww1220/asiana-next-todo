import Document, { DocumentContext, Html, Head, Main, NextScript } from 'next/document';

// functional component 는 아직 지원하지 않는듯,, 상속으로 메서드를 실행시키야됨
class MyDocument extends Document {
    static async getInitialProps(ctx: DocumentContext) {
        const initialProps = await Document.getInitialProps(ctx);
        return { ...initialProps };
    }

    render() {
        return (
            <Html>
                <Head />
                <body>
                    <Main />
                    {/*}<div id="modal-root"></div>{*/}
                    <NextScript />
                </body>
            </Html>
        );
    }
}

export default MyDocument;
