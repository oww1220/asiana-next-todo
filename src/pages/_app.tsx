import { useEffect } from 'react';
import { AppProps, AppContext } from 'next/app';

import Layout from '@src/components/Layout/Layout';
import * as Commons from '@src/lib/Commons';

//redux
import Wrapper from '@src/store/ConfigureStore';

//apollo
import { ApolloProvider } from '@apollo/client';
import { useApollo } from '@src/store/ConfigureClient';

import '@src/assets/scss/Root.scss';

const App = ({ Component, pageProps, router }: AppProps) => {
  console.log(
      `%c-------------------------------- (${Component.name} : ${router.pathname}) --------------------------------`,
      Commons.consoleStyle00,
  );

  const { initialApolloState } = pageProps;

  const apolloClient = useApollo(initialApolloState);


  return (
    <ApolloProvider client={apolloClient}>
      <Layout>
        <Component {...pageProps} />
      </Layout>
    </ApolloProvider>
    
  )
    
};

export default Wrapper.withRedux(App);
