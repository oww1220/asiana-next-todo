import Head from 'next/head';
import React from 'react';
import styled, { createGlobalStyle } from 'styled-components';

const IeCopyWrapper = styled.div`
    width:100%;height:100vh;
    textarea {padding:15px;font-family:monaco;font-size:16px;box-sizing: border-box;border:0;width:100%;height:100%;}
`;

const GlobalStyle = createGlobalStyle`
    html, body, textarea {margin:0;padding:0;overflow:hidden;width:100%;height:100%;color:#fff;background-color:#222;}
    #root {width:100%;height:100%;}
`;

const IeCopyWrap = ()=> {
	return (
        <>
            <Head>
                <title>IE COPY</title>
            </Head>
            <GlobalStyle/>
            <IeCopyWrapper>
            <textarea></textarea>
            </IeCopyWrapper>

        </>
	)
}

export default IeCopyWrap;
