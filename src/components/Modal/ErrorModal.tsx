import { useRouter } from 'next/router';
import React, { useState, useCallback, useEffect } from 'react';
import { GraphQLError } from 'graphql';
import Modal from '@src/components/Modal/Modal';
import * as Commons from '@src/lib/Commons';

interface IErrorModal {
    errorData: any;
    errorCallBack?: () => any;
}


export const ErrorModal = ({ errorData, errorCallBack }: IErrorModal) => {
    console.log('call:', ErrorModal.name, '!!');
    const router = useRouter();
    const chkAuth = errorData.length && errorData.findIndex((errors) => errors.extensions.code === 'UNAUTHENTICATED');
    //graphql은 에러를 배열로 내려줌!!! 쿼리를 묶어서 처리가 가능해서 각 쿼리 에러들을 배열로 받음!
    //console.log(`%c errorModal ${chkAuth}`, Commons.consoleStyle03, errorData);
    //console.log('비교', currentData===errorData);
    //currentData = errorData;

    const errorLogin = () => {
        //인증에러시에 모든 모달 닫기버튼에 페이지 reload시켜서 refreshToken 서버로 보내 체크시킴!
        //router.push('/LogIn');
        window.location.reload();
    };

    return (
        <Modal
            content={
                <div>{<p>{errorData.message}</p>}</div>
            }
            buttonVisible={false}
            classFlag={'errorModal'}
            dependence={errorData}
            cssStyle={{ width: '400px', background: '#fff' }}
            callback={chkAuth !== -1 ? errorLogin : errorCallBack}
            chkAuth={chkAuth}
        />
    );
};

export default ErrorModal;
