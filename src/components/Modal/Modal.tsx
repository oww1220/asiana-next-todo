import React, { useCallback, useEffect, useState, MouseEvent } from 'react';
import cx from 'classnames';

interface IModal {
    content: JSX.Element;
    classFlag?: string;
    closable?: boolean;
    buttonVisible?: boolean;
    maskClosable?: boolean;
    cssStyle?: React.CSSProperties;
    dependence?: any;
    callback?: () => any;
    chkAuth?: number;
}

export const Modal = ({
    content,
    classFlag = '',
    closable = true,
    buttonVisible = true,
    dependence = null,
    maskClosable = true,
    cssStyle = undefined,
    callback = undefined,
    chkAuth = -1,
}: IModal) => {
    //console.log('call:', Modal.name, '!!');

    //모달 visible state
    const [modalVisible, setModalVisible] = useState(!buttonVisible);

    //모달 열기
    const openModal = useCallback(() => {
        setModalVisible(true);
    }, []);

    //모달 닫기
    const closeModal = useCallback((e) => {
        setModalVisible(false);
    }, []);

    //전체 닫기 핸들러
    const onMaskClick = useCallback((e: MouseEvent) => {
        if (maskClosable && e.target === e.currentTarget) {
            //인증에러시에 모든 모달 닫기버튼에 페이지 reload시켜서 refreshToken 서버로 보내 체크시킴!
            if (chkAuth !== -1 && callback) callback();
            closeModal(e);
        }
    }, []);

    //닫기 핸들러
    const close = useCallback((e: MouseEvent) => {
        //인증에러시에 모든 모달 닫기버튼에 페이지 reload시켜서 refreshToken 서버로 보내 체크시킴!
        if (chkAuth !== -1 && callback) callback();
        closeModal(e);
    }, []);

    //콜백 실행후 닫기
    const callbackClose = useCallback((e: MouseEvent) => {
        if (callback) {
            callback();
            closeModal(e);
        }
    }, []);

    //버튼 없는 모달은 바로 열려야 되서(부모에서 액션 값을 받아서 씀)
    //리듀서로 액션을 통해 나오는 데이터는 불변(고유)의 성질을 가져서...데이터는 같을지라도 구조체는 얕은비교(참조값:주소값)를 하기 때문에 {이전값} !== {최신값}임
    useEffect(() => {
        //console.log('dependence!!', dependence);
        if (!buttonVisible) {
            openModal();
        }
    }, [dependence]);

    //모달상태가 바뀔때마다 이팩트 하지만...모달 visible state true일시만 로직적용(body스크롤 막고 풀기 로직)
    useEffect(() => {
        if (modalVisible) {
            document.body.style.cssText = `position: fixed; overflow: hidden; top: -${window.scrollY}px`;
            return () => {
                console.log('clear callback!');
                const scrollY = document.body.style.top;
                document.body.style.removeProperty('position');
                document.body.style.removeProperty('overflow');
                document.body.style.setProperty('top', '0');
                window.scrollTo(0, parseInt(scrollY || '0') * -1);
            };
        }
    }, [modalVisible]);

    return (
        <>
            {/*buttonVisible에 따라 버튼 유무*/}
            {buttonVisible && (
                <button className="modal-open-btn btn" type="button" onClick={openModal}>
                    레이어 열기
                </button>
            )}
            
            {modalVisible && (
                <>
                    <div
                        className={cx([`${classFlag}Wrapper`, 'modalWrapper'], { active: modalVisible })}
                        onClick={onMaskClick}
                        tabIndex={-1}
                    >
                        <div className="modalInner" tabIndex={0} style={cssStyle}>
                            {closable && (
                                <button className="modal-close" onClick={close}>
                                    레이어 닫기
                                </button>
                            )}
                            <div className="layer-in">
                                <div className="normal-cont">
                                    {content}
                                    {callback && (
                                        <button type="button" className="confirm-bt" onClick={callbackClose}>
                                            확인
                                        </button>
                                    )}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className={cx([`${classFlag}Wrapper`, 'modalOverlay'], { active: modalVisible })} />
                </>
            )}
        </>
    );
};

export default Modal;
