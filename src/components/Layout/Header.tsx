import Link from 'next/link';
import { useRouter } from 'next/router';
import React, { useEffect, MouseEvent } from 'react';
import Datepicker from '@src/components/Input/Datepicker';
import jquery from 'jquery';

import Config from '@src/lib/Config';
import useChange from '@src/store/hooks/UseChange';
import useList from '@src/store/hooks/UseList';
import useWrite from '@src/store/hooks/UseWrite';
import ButtonHover from '@src/components/ButtonHover/ButtonHover';
const $: JQueryStatic = jquery;

const Header = ()=>{
    const router = useRouter();
    const { changeDate } = useChange();
    const { getListDefault } = useList();
    const { getWriteReset } = useWrite();

    const mainClick = (e: MouseEvent) => {
        let tg = $('#container .task-board');
        $('#search').val('');
        tg.find('.chk').each(function () {
            $(this).html(
                $(this)
                    .html()
                    .replace(/<span class="highLight".*?>(.*?)/gi, '')
                    .replace(/<\/span>/gi, '')
            );
        });
        router.push('/');
        changeDate(new Date());
        getListDefault();
        e.preventDefault();
    };

    const taskClcik = (e: MouseEvent) => {
        router.push('/Write');
        //getWriteReset();
        e.preventDefault();
    };

    /*
    useEffect(() => {
        console.log(location.pathname);
    }, [location.pathname]);
    */

    return (
        <header id="header">
            <h1>
                <a href="#" onClick={mainClick}>
                    <span className="name-main">ASIANA</span>{' '}
                    <span className="name-sub">
                        <b>AIRLINES</b>
                    </span>{' '}
                </a>
            </h1>
            <span className="app-version">v.{Config.APP_VERSION}</span>
            {router.pathname.indexOf('/Write') === -1 && <Datepicker />}
            <div className="langs-link">
                <h2>현황판:</h2>
                <nav>
                    <a href="#" onClick={taskClcik} className="btn-data-write">
                        task 등록
                    </a>
                </nav>
            </div>
            <div className="function_wrap">
                <h2>업무 유틸리티:</h2>
                <Link href="/IeCopyWrap">
                    <a target="_blank" rel="noreferrer">IE COPY</a>
                </Link>
                <Link href="/MapToAnchor">
                    <a target="_blank" rel="noreferrer">Anchor 변환</a>
                </Link>
                <Link href="/UnEscape">
                    <a target="_blank" rel="noreferrer">UnEscape</a>
                </Link>
                <Link href="/VwCalculator">
                    <a target="_blank" rel="noreferrer">PX to VW 변환</a>
                </Link>
                <Link href="/CkEditor">
                    <a target="_blank" rel="noreferrer">CkEditor</a>
                </Link>
                <Link href="/SortSource">
                    <a target="_blank" rel="noreferrer">HTML/JS 코드정렬</a>
                </Link>
                <a href="./ImageMapGenrator.html" target="_blank">
                    이미지맵
                </a>
                <ButtonHover>
                    <a href="http://www.lonniebest.com/FormatCSS/" target="_blank">
                        CSS 코드정렬
                    </a>
                </ButtonHover>
            </div>
        </header>
    );
}

export default Header;
