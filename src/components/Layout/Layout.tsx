import Head from 'next/head';
import React, { useEffect } from 'react';
import RouteProvider from '@src/provider/RouteProvider';
import LoadingProvider from '@src/provider/LoadingProvider';
import ErrorProvider from '@src/provider/ErrorProvider';
//import LoginCheckProvider from '@src/provider/LoginCheckProvider';
import * as Commons from '@src/lib/Commons';


interface ILayoutProps {
    children: JSX.Element;
    //refreshTokenFlag: boolean;
    //refreshTokenUpdateFlag: boolean;
}

const Layout = ({ children }: ILayoutProps) => {
    //console.log('%c children', 'color:blue;', children);

    useEffect(() => {
        //rem 계산
        Commons.remCaculate();
        window.addEventListener('resize', Commons.remCaculate);
        return () => {
            //console.log('clear!!!!!!!!!');
        };
    }, []);

    return (
        <LoadingProvider>
            <ErrorProvider>
                    <RouteProvider>
                        <>
                            <Head>
                                <title>Aisana Airlines 업무현황판</title>
                                <link rel="icon" href="./static/images/favicon-job.ico" />
                            </Head>
                            {children}
                        </>
                    </RouteProvider>
            </ErrorProvider>
        </LoadingProvider>
    );
};

export default Layout;
