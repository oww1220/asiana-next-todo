import React, { useState, useEffect } from 'react';
import { Controller, Control, OmitResetState } from 'react-hook-form';
import styled from 'styled-components';
import DatePicker, { registerLocale } from 'react-datepicker';
import useWrite from '@src/store/hooks/UseWrite';
import { ListState } from 'Type_resData';
import * as Commons from '@src/lib/Commons';
import ko from 'date-fns/locale/ko';
import 'react-datepicker/dist/react-datepicker.css';

registerLocale('ko', ko);

interface DatepickerInputProps {
    control: Control<ListState>;
    reset(values?: Record<string, any>, omitResetState?: OmitResetState): void;
    setValue(name: string, value: any, config?: Object): void;
    getValues(payload?: string | string[]): Object;
    register(Ref?: any, validateRule?: any): void;
    name: string;
    target: string;
}

const DatepickerInputWrapper = styled.div`
    display: inline-block;
    .react-datepicker-wrapper {
        width: 100%;
    }
    .real {
        display: none;
    }
`;

const DatepickerInput = ({ control, reset, target, setValue, getValues, name, register }: DatepickerInputProps)=> {
    const { listState } = useWrite();
    const [currentDate, setCurrentDate] = useState(new Date());

    useEffect(() => {
        const day = new Date(target);
        //console.log(day);
        setCurrentDate(day);
    }, [listState]);

    return (
        <DatepickerInputWrapper>
            <Controller
                as={DatePicker}
                name={`${name}1`}
                selected={currentDate}
                onChange={([date]: any) => {
                    const day = Commons.dateCheck(date)[0];
                    //reset({[name]: day});
                    setCurrentDate(date);
                    setValue(name, day);
                    if (name === 'date') setValue('day', Commons.dateCheck(date)[1]);
                    //console.log(date, day, getValues(name));
                }}
                dateFormat={'yyyy-MM-dd'}
                locale={'ko'}
                control={control}
            />
            <input type={'text'} ref={register} name={name} className={'real'} />
        </DatepickerInputWrapper>
    );
}

export default DatepickerInput;
