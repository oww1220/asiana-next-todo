const Config = {
    BEARERTOKEN: 'BearerToken',
    COOKIES_TIME: 15 * 60 * 1000, //15분
    BASE_URL: process.env.NEXT_PUBLIC_APP_BASE_URL,
    APP_ENV: process.env.NEXT_PUBLIC_APP_ENV,
    APP_VERSION: process.env.NEXT_PUBLIC_APP_VERSION,
    GOOGLE_CLIENT_ID: process.env.NEXT_PUBLIC_GOOGLE_CLIENT_ID,
    APP_NAME: process.env.NEXT_PUBLIC_APP_NAME, //클라이언트에서 사용하기 위해서는 NEXT_PUBLIC_ 앞에 붙이야됨
    APP_SERVERONLY: process.env.APP_SERVERONLY, //서버에서만 사용
};

export default Config;
