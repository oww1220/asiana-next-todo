import { useRouter } from 'next/router';
import React from 'react';
import { ResetGlobalStyle } from '@src/lib/Reset';
import Header from '@src/components/Layout/Header';
import BtTop from '@src/components/BtTop/BtTop';
import ScrollToTop from '@src/components/ScrollToTop/ScrollToTop';


interface IRouteProviderProps {
    children: JSX.Element;
}

const RouteProvider = ({ children }: IRouteProviderProps) => {
    console.log('call:', RouteProvider.name, '!!');
    const router = useRouter();

    return (
        <>
            <ScrollToTop />
            {!(
                router.pathname === '/CkEditor' ||
                router.pathname === '/VwCalculator' ||
                router.pathname === '/SortSource'
            ) && <ResetGlobalStyle />}
            {router.pathname !== '/IeCopyWrap' &&
                router.pathname !== '/MapToAnchor' &&
                router.pathname !== '/VwCalculator' &&
                router.pathname !== '/CkEditor' &&
                router.pathname !== '/SortSource' &&
                router.pathname !== '/UnEscape' ? (
                    <>
                        <Header />
                        <div id="container">
                            {children}
                        </div>
                    </>
                ) : (
                    children
            )}
            <BtTop />
        </>
    );
};

export default RouteProvider;
