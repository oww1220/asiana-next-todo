import { useRouter } from 'next/router';
import { useCallback, useEffect } from 'react';
import useError from '@src/store/hooks/UseError';
//import useLogIn from '@src/store/hooks/UseLogIn';
import ErrorModal from '@src/components/Modal/ErrorModal';
import * as Commons from '@src/lib/Commons';
import Config from '@src/lib/Config';

interface IErrorProvideProps {
    children: JSX.Element;
}

const ErrorProvider = ({ children }: IErrorProvideProps) => {
    console.log('call:', ErrorProvider.name, '!!');
    //alert('errorProvider');

    //끌어온 프라퍼티 말고 다른 프라퍼티가 업그레이드 되어도.. 상태변화로 인식하여 리랜더 됨
    const router = useRouter();
    const { errorData, errorCallBack, failureLoad } = useError();
    //const { linkLogOutSuccess } = useLogIn(); //구독되어 있어서 삭제시킴 컴퍼넌트가 쓸데없이 호출됨;;;

    //errorData 바뀔시에만 함수 새로 생성
    const errorHandler = useCallback(() => {
        //graphql은 에러를 배열로 내려줌!!! 쿼리를 묶어서 처리가 가능해서 각 쿼리 에러들을 배열로 받음!
        console.log('errorHandler!!:', errorData);

        //에러객체들을 순회해서 인증코드가 하나라도 걸리면 accessToken삭제!
        const chkAuth =
            errorData.length && errorData.findIndex((errors) => errors.extensions.code === 'UNAUTHENTICATED');
        //console.log('chk!!!!!', chkAuth);
        //인증 오류시 accessToken삭제
        if (chkAuth !== -1) {
            console.log(`%c (${chkAuth}) accessToken인증 오류!! accessToken삭제`, Commons.consoleStyle02);
            //accessToken삭제!
            Commons.removeCookies(Config.BEARERTOKEN);
            //linkLogOutSuccess();
        }
    }, [errorData]);

    // 사용자가 새 URL을 탐색할 때 마다 이 상태코드를 "제거" 해야한다. 그렇지 않을 경우 사용자는 오류 페이지에 영원히 "갇히게" 된다.
    useEffect(() => {
        console.log('errorEffect!!', errorData);
        if (errorData) {
            errorHandler();
            // 현재 위치의 변경 사항을 하는 리스너
            //const unlisten = history.listen(() => failureLoad(null));
            const handleRouteChange = (url, { shallow }) => {
                console.log(
                    `%c App is changing to ${url} ${
                        shallow ? 'with' : 'without'
                    } shallow routing -- errorData nulling!`,
                    Commons.consoleStyle02,
                );
                failureLoad(null);
            };

            router.events.on('routeChangeStart', handleRouteChange);

            // unmount될 때 리스너 제거
            return () => {
                console.log('error 초기화');
                router.events.off('routeChangeStart', handleRouteChange);
                //unlisten();
            };

            //테스트 결과 cleanup 함수는 .... 컴퍼넌트가 다음 시점(store state가 변경되는)이 렌더링(함수실행) 되고나서 실행--이 시점에 리덕스 store state update(action)을 할경우 한번 더 함수호출 하여 이시점 상태가 최종으로 마운트됨...즉 클린업 함수는 새로운 돔 구성하기 전에 실행이됨...

            //effect 함수는 해당 컴퍼넌트가 마운트 되고 나서(브라우저가 페인트되고 나서) 실행됨,,
            //effect 함수 내부에 cleanup 함수는 이펙트 함수 실행후에 실행되는게 아님.. 다음 시점(store state가 변경되는)으로 미뤄짐 그리고 다음상태가 업데이트 되고나서 실행됨
            //처음 컴퍼넌트 호출 --> 이팩트 확인--> 돔 업데이트 --> 이팩트 실행(이팩트함수 있으면) -----(액션)-----> 상태변경됨 --> 컴퍼넌트 호출(구독이 되었다고 가정) --> 이팩트 확인(의존성 배열에 따라 변경이 잇으면 큐에 함수 집어넣음) --> 이전 클린업함수 호출 --> 돔업데이트 --> 이팩트실행(이팩트함수 있으면)-----------계속 반복 :해당 컴퍼넌트가 언마운트 되기 전까지...

            //store state가 변경되면 해당 state를 구독하는 컴퍼넌트는 업데이트 대상이 되어서 리렌더링(함수실행)됨
            //error프로바이더는 항상 마운트 되어 있기때문에...컴퍼넌트가 변경되었을 경우만 실행됨..(클린업 함수가 실행되는 경우 2가지 중: 마운트 해제 or 컴퍼넌트가 다음 시점(store state가 변경되는)이 렌더링(함수실행) 되고나서 )
            //return ()=> {
            //console.log('error 초기화');
            //failureLoad(null);
            //}
        }
    }, [errorHandler]); //의존성 배열--함수자체로 포인터 값이 들어간다고 생각하면됨

    return (
        <>
            {children}
            {errorData && <ErrorModal errorData={errorData} errorCallBack={errorCallBack} />}
        </>
    );
};

export default ErrorProvider;
