import { createStore, applyMiddleware, Store } from 'redux';
import createSagaMiddleware, { END, SagaMiddleware, Task } from 'redux-saga';
import { composeWithDevTools } from 'redux-devtools-extension';
import { createWrapper, Context } from 'next-redux-wrapper';
import rootReducer, { State } from '@src/store/reducer/RootReducer';
import rootSaga from '@src/store/saga/RootSaga';
import Config from '@src/lib/Config';

const configureStore = (preloadedState) => {
    //console.log('preloadedState!!', preloadedState);
    const sagaMiddleware = createSagaMiddleware();
    const middlewares = [sagaMiddleware];

    //dev일시만 redux-devtools-extension 합성!
    const enhancer = Config.APP_ENV ==='dev' ? composeWithDevTools(applyMiddleware(...middlewares)): applyMiddleware(...middlewares);
    const store = createStore(rootReducer, preloadedState, enhancer);
    store.sagaTask = sagaMiddleware.run(rootSaga);
    store.close = () => store.dispatch(END);
    return store;
};

const Wrapper = createWrapper(configureStore, { debug: false });

export default Wrapper;
