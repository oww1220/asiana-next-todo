import { HYDRATE } from 'next-redux-wrapper';
import { ListState, LangsLink } from 'Type_resData';
import { State } from '@src/store/reducer/RootReducer';

/*액션상수*/
export const GET_LIST = 'ListReducer/GET_LIST' as const;
export const GET_LIST_DEFAULT = 'ListReducer/GET_LIST_DEFAULT' as const;
export const GET_LIST_SUCCESS = 'ListReducer/GET_LIST_SUCCESS' as const;

export const GET_LIST_MODAL = 'ListReducer/GET_LIST_MODAL' as const;
export const GET_LIST_MODAL_SUCCESS = 'ListReducer/GET_LIST_MODAL_SUCCESS' as const;

export const GET_LIST_MODAL_MONTH = 'ListReducer/GET_LIST_MODAL_MONTH' as const;
export const GET_LIST_MODAL_MONTH_SUCCESS = 'ListReducer/GET_LIST_MODAL_MONTH_SUCCESS' as const;

export const CHANGE_SEARCH = 'ListReducer/CHANGE_SEARCH' as const;
export const CHANGE_CASHEND = 'ListReducer/CHANGE_CASHEND' as const;
export const SEARCH_LIST = 'ListReducer/SEARCH_LIST' as const;
export const SEARCH_LIST_RESET = 'ListReducer/SEARCH_LIST_RESET' as const;
export const SEARCH_LIST_END = 'ListReducer/SEARCH_LIST_END' as const;
export const SEARCH_LIST_SUCCESS = 'ListReducer/SEARCH_LIST_SUCCESS' as const;

/*액션함수*/
/*리스트 불러오기*/
export const getList = (date: Date) => ({ type: GET_LIST, payload: { date } });
export const getListDefault = () => ({ type: GET_LIST_DEFAULT });
export const getListSuccess = (data: ListState[]) => ({ type: GET_LIST_SUCCESS, payload: { data } });

/*주간보기*/
export const getModalList = (range: string[]) => ({ type: GET_LIST_MODAL, payload: { range } });
export const getModalListSuccess = (data: ListState[], range: string[]) => ({
    type: GET_LIST_MODAL_SUCCESS,
    payload: { data, range },
});

/*월간보기*/
export const getModalListMonth = (date: Date) => ({ type: GET_LIST_MODAL_MONTH, payload: { date } });
export const getModalListMonthSuccess = (data: ListState[]) => ({
    type: GET_LIST_MODAL_MONTH_SUCCESS,
    payload: { data },
});

/*검색*/
export const changeSearch = (searchStaus: boolean) => ({ type: CHANGE_SEARCH, payload: { searchStaus } });
export const changeCashEnd = (cashEnd: number) => ({ type: CHANGE_CASHEND, payload: { cashEnd } });
export const searchList = (
    txt: string,
    startIdx: number,
    endIdx: number,
    startDate: Date | null,
    endDate: Date | null,
    callback: (val: string) => any,
) => ({
    type: SEARCH_LIST,
    payload: { txt, startIdx, endIdx, startDate, endDate, callback },
});
export const searchListReset = () => ({ type: SEARCH_LIST_RESET });
export const searchListEnd = (searchEnd: boolean) => ({ type: SEARCH_LIST_END, payload: { searchEnd } });
export const searchListSuccess = (data: ListState[], startIdx: number, endIdx: number) => ({
    type: SEARCH_LIST_SUCCESS,
    payload: { data, startIdx, endIdx },
});

/*기본상태 및 리듀서*/
export interface IListState {
    listState: ListState[];
    listModalState: ListState[];
    range: string[];
    startIdx: number;
    endIdx: number;
    searchStaus: boolean;
    searchEnd: boolean;
    cashEnd: number;
}

const initialState = {
    listState: [],
    listModalState: [],
    range: [],
    startIdx: 0,
    endIdx: 0,
    searchStaus: false,
    searchEnd: false,
    cashEnd: 0,
};

type ListAction = 
| ReturnType<typeof getList>
| ReturnType<typeof getListDefault>
| ReturnType<typeof getListSuccess>
| ReturnType<typeof getModalList>
| ReturnType<typeof getModalListSuccess>
| ReturnType<typeof getModalListMonth>
| ReturnType<typeof getModalListMonthSuccess>
| ReturnType<typeof changeSearch>
| ReturnType<typeof changeCashEnd>
| ReturnType<typeof searchList>
| ReturnType<typeof searchListReset>
| ReturnType<typeof searchListEnd>
| ReturnType<typeof searchListSuccess>;

export const ListReducer = (
    state: IListState = initialState,
    action: ListAction | { type: typeof HYDRATE; payload: State },
)=> {
    switch (action.type) {
        case HYDRATE:
            return {
                ...state,
                ...action.payload.ListReducer,
            };
            case GET_LIST:
                return {
                    ...state,
                    searchEnd: false,
                    loadState: false,
                };
    
            case GET_LIST_DEFAULT:
                return {
                    ...state,
                    searchEnd: false,
                    loadState: false,
                };
            case GET_LIST_SUCCESS:
                return {
                    ...state,
                    listState: action.payload.data,
                    searchStaus: false,
                    loadState: false,
                };

            case GET_LIST_MODAL:
                return {
                    ...state,
                    searchEnd: false,
                    loadState: false,
                };
    
            case GET_LIST_MODAL_SUCCESS:
                return {
                    ...state,
                    listModalState: action.payload.data,
                    range: action.payload.range,
                    searchStaus: false,
                    loadState: false,
                };

            case GET_LIST_MODAL_MONTH:
                return {
                    ...state,
                    searchEnd: false,
                    loadState: false,
                };
            case GET_LIST_MODAL_MONTH_SUCCESS:
                return {
                    ...state,
                    listModalState: action.payload.data,
                    searchStaus: false,
                    loadState: false,
                };


        case CHANGE_SEARCH:
            return {
                ...state,
                searchStaus: action.payload.searchStaus,
            };

        case CHANGE_CASHEND:
            return {
                ...state,
                cashEnd: action.payload.cashEnd,
            };
    
            case SEARCH_LIST_RESET:
                return {
                    ...state,
                    startIdx: 0,
                    endIdx: 0,
                    cashEnd: 0,
                    listState: [],
                };
    
            case SEARCH_LIST_END:
                return {
                    ...state,
                    searchEnd: action.payload.searchEnd,
                };
    
            case SEARCH_LIST_SUCCESS:
                return {
                    ...state,
                    listState: [...state.listState, ...action.payload.data],
                    startIdx: action.payload.startIdx,
                    endIdx: action.payload.endIdx,
                    searchStaus: true,
                };

        default:
            return state;
    }
}
