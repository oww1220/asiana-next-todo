import { combineReducers } from 'redux';

import { LoadReducer, ILoadState } from '@src/store/reducer/LoadReducer';
import { ChangeReducer, IChangeState } from '@src/store/reducer/ChangeReducer';
import { ErrorReducer, IErrorState } from '@src/store/reducer/ErrorReducer';
import { ListReducer, IListState } from '@src/store/reducer/ListReducer';
import { WriteReducer, WriteState } from '@src/store/reducer/WriteReducer';

export type State = {
    LoadReducer: ILoadState;
    ChangeReducer: IChangeState;
    ErrorReducer: IErrorState;
    ListReducer: IListState;
    WriteReducer: WriteState;
};

const rootReducer = combineReducers({
    LoadReducer,
    ErrorReducer,
    ChangeReducer,
    ListReducer,
    WriteReducer,
});

export default rootReducer;

