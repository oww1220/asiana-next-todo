import { HYDRATE } from 'next-redux-wrapper';
import { State } from '@src/store/reducer/RootReducer';

/*액션상수*/
export const CHANGE_DATE = 'ChangeReducer/CHANGE_DATE' as const;


/*액션함수*/
export const changeDate = (date: Date) => ({ type: CHANGE_DATE, payload: { date } });

/*기본상태 및 리듀서*/
export interface IChangeState {
    dateState: Date,
}

const initialState = {
    dateState: new Date(),
};

type ChangeAction = ReturnType<typeof changeDate>;

export const ChangeReducer = (
    state: IChangeState = initialState,
    action: ChangeAction | { type: typeof HYDRATE; payload: State },
)=> {
    switch (action.type) {
        case HYDRATE:
            return {
                ...state,
                ...action.payload.ChangeReducer,
            };

        case CHANGE_DATE:
            return {
                ...state,
                dateState: action.payload.date,
            };
    

        default:
            return state;
    }
}
