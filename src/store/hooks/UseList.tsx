import React, { useCallback } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { bindActionCreators } from 'redux';

import { State } from '@src/store/reducer/RootReducer';
import {
    changeSearch,
    getList,
    getListDefault,
    getModalList,
    getModalListMonth,
    searchList,
    searchListReset,
    searchListEnd,
    changeCashEnd,
} from '@src/store/reducer/ListReducer';

export const selectListState = (state: State) => state.ListReducer.listState;
export const selectListModalState = (state: State) => state.ListReducer.listModalState;
export const selectRange = (state: State) => state.ListReducer.range;
export const selectStartIdx = (state: State) => state.ListReducer.startIdx;
export const selectEndIdx = (state: State) => state.ListReducer.endIdx;
export const selectSearchStaus = (state: State) => state.ListReducer.searchStaus;
export const selectSearchEnd = (state: State) => state.ListReducer.searchEnd;
export const selectCashEnd = (state: State) => state.ListReducer.cashEnd;

const UseList = ()=> {
    const dispatch = useDispatch();

    return {
        listState: useSelector(selectListState),
        listModalState: useSelector(selectListModalState),
        range: useSelector(selectRange),
        startIdx: useSelector(selectStartIdx),
        endIdx: useSelector(selectEndIdx),
        searchStaus: useSelector(selectSearchStaus),
        searchEnd: useSelector(selectSearchEnd),
        cashEnd: useSelector(selectCashEnd),
        changeSearch: useCallback(bindActionCreators(changeSearch, dispatch), [dispatch]),
        getList: useCallback(bindActionCreators(getList, dispatch), [dispatch]),
        getListDefault: useCallback(bindActionCreators(getListDefault, dispatch), [dispatch]),
        getModalList: useCallback(bindActionCreators(getModalList, dispatch), [dispatch]),
        getModalListMonth: useCallback(bindActionCreators(getModalListMonth, dispatch), [dispatch]),
        searchList: useCallback(bindActionCreators(searchList, dispatch), [dispatch]),
        searchListReset: useCallback(bindActionCreators(searchListReset, dispatch), [dispatch]),
        searchListEnd: useCallback(bindActionCreators(searchListEnd, dispatch), [dispatch]),
        changeCashEnd: useCallback(bindActionCreators(changeCashEnd, dispatch), [dispatch]),
    };
}

export default UseList;
