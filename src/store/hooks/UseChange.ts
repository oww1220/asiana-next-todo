import React, { useCallback } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { bindActionCreators } from 'redux';
import { State } from '@src/store/reducer/RootReducer';
import { changeDate } from '@src/store/reducer/ChangeReducer';

export const selectDateState = (state: State) => state.ChangeReducer.dateState;

const UseChange = () => {
    const dispatch = useDispatch();

    return {
        dateState: useSelector(selectDateState),
        changeDate: useCallback(bindActionCreators(changeDate, dispatch), [dispatch]),
    };
};

export default UseChange;
