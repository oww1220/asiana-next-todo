import { useState, useEffect } from 'react';
import useList from '@src/store/hooks/UseList';

const useInfiniteScroll = (callback?: ()=>any) => {
    const { searchEnd, searchStaus } = useList();
    const [isFetching, setIsFetching] = useState(false);

    const handleScroll = ()=> {
        if (searchStaus) {
            if (
                window.innerHeight + document.documentElement.scrollTop + 5 <= document.documentElement.offsetHeight ||
                isFetching
            )
                return;
            setIsFetching(true);
        }
    };

    useEffect(() => {
        window.addEventListener('scroll', handleScroll);
        return () => window.removeEventListener('scroll', handleScroll);
    }, [searchStaus]);

    /*
    useEffect(() => {
        console.log('isFetching:', isFetching);
    });
    */

    useEffect(() => {
        //console.log('!!!!콜백')
        console.log('isFetching', isFetching);

        if (!isFetching) return;
        if (searchEnd) return;

        callback();
    }, [isFetching]);


    return { isFetching, setIsFetching };
};

export default useInfiniteScroll;
