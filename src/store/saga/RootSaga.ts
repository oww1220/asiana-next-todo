import { all, fork } from 'redux-saga/effects';


import { watchGetListDefault } from '@src/store/saga/ListSaga';
//import { watchGetList, watchGetListDefault, watchGetModalList, watchGetModalListMonth } from '@src/store/saga/ListSaga';
//import { watchSearchList } from '@src/store/saga/SearchSaga';
//import { watchGetWrite, watchPostWrite, watchPutWrite } from '@src/store/saga/WriteSaga';
//import { watchFetchTestList, watchFetchAuthList } from '@src/store/saga/ListSaga';
//import { watchLinkSignUp } from '@src/store/saga/SignUpSaga';
//import { watchLinkLogIn, watchLinkLogOut, watchUpdateRefreshToken } from '@src/store/saga/LogInSaga';
//import { watchUpdateUser, watchDeleteUser } from '@src/store/saga/UpdateSaga';

export default function* rootSaga() {
    yield all([
        fork(watchGetListDefault),
        //fork(watchLinkSignUp),
        //fork(watchLinkLogIn),
        //fork(watchLinkLogOut),
        //fork(watchFetchAuthList),
        //fork(watchUpdateUser),
        //fork(watchDeleteUser),
        //fork(watchUpdateRefreshToken),
    ]);
}
