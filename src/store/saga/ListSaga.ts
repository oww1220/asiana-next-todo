import { takeEvery, put, call } from 'redux-saga/effects';

import * as Api from '@src/lib/Api';
import * as ActionsLoad from '@src/store/reducer/LoadReducer';
import * as ActionsList from '@src/store/reducer/ListReducer';
import * as ActionsError from '@src/store/reducer/ErrorReducer';

function* getListDefaultSaga(action: ReturnType<typeof ActionsList.getListDefault>) {
    console.log('saga!:', action);

    yield put(ActionsLoad.showLoad());

    try {
        const { data } = yield call(Api.getListDefault);
        console.log(data);
        yield put(ActionsList.getListSuccess(data));
        yield put(ActionsLoad.hideLoad());
        //throw new Error('에러 테스트!');
    } catch (error) {
        //console.log(error)
        yield put(ActionsLoad.hideLoad());
        yield put(ActionsError.failureLoad(error));
    }
}


/*
function* getListSaga(action: ReturnType<typeof ActionsLoad.getList>) {
    console.log('saga!:', action);

    yield put(ActionsLoad.showLoad());

    try {
        const dateState = action.payload.date;
        const { data } = yield call(Api.getList, dateState);
        console.log(data);
        yield put(ActionsLoad.getListSuccess(data));
    } catch (error) {
        yield put(ActionsLoad.getListFailure(error.response));
    }
}


function* getModalListSaga(action: ReturnType<typeof ActionsLoad.getModalList>) {
    console.log('saga!:', action);

    yield put(ActionsLoad.showLoad());

    try {
        const range = action.payload.range;
        const { data } = yield call(Api.getListModalItem, range);
        console.log(data);
        yield put(ActionsLoad.getModalListSuccess(data, range));
    } catch (error) {
        yield put(ActionsLoad.getModalListFailure(error.response));
    }
}

function* getModalListMonthSaga(action: ReturnType<typeof ActionsLoad.getModalListMonth>) {
    console.log('saga!:', action);

    yield put(ActionsLoad.showLoad());

    try {
        const date = action.payload.date;
        const { data } = yield call(Api.getListModalItemMonth, date);
        console.log(data);
        yield put(ActionsLoad.getModalListMonthSuccess(data));
    } catch (error) {
        yield put(ActionsLoad.getModalListMonthFailure(error.response));
    }
}
*/

export function* watchGetListDefault() {
    yield takeEvery(ActionsList.GET_LIST_DEFAULT, getListDefaultSaga);
}

/*
export function* watchGetList() {
    yield takeEvery(ActionsList.GET_LIST, getListSaga);
}

export function* watchGetModalList() {
    yield takeEvery(ActionsList.GET_LIST_MODAL, getModalListSaga);
}

export function* watchGetModalListMonth() {
    yield takeEvery(ActionsList.GET_LIST_MODAL_MONTH, getModalListMonthSaga);
}*/
